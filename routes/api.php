<?php

Route::group(['as' => 'api.'], function () {

    Route::group(['as' => 'auth.'], function () {
        Route::post('/auth/login', 'Api\Auth\LoginController@login')->name('login');
        Route::post('/auth/logout', 'Api\Auth\LoginController@logout')->name('logout');
    });

    Route::middleware('jwt')->group(function () {
        Route::group(['as' => 'tasks.'], function () {
            Route::get('/tasks', 'Api\Task\TaskController@index')->name('index');
            Route::get('/tasks/completed', 'Api\Task\CompletedTaskController@index')->name('completed');
            Route::post('/tasks', 'Api\Task\TaskController@store')->name('store');
            Route::patch('/tasks/{task}/title', 'Api\Task\TaskController@updateTitle')->name('update.title');
            Route::patch('/tasks/{task}/type', 'Api\Task\TaskController@updateType')->name('update.type');
            Route::post('/tasks/sort', 'Api\Task\TaskSortController@update')->name('sort');
            Route::delete('/tasks/{task}', 'Api\Task\TaskController@destroy')->name('destroy');
        });

        Route::group(['as' => 'affairs.'], function () {
            Route::get('/affairs', 'Api\Affair\AffairController@index')->name('index');
            Route::post('/affairs', 'Api\Affair\AffairController@store')->name('store');
            Route::patch('/affairs/{affair}/title', 'Api\Affair\AffairController@updateTitle')->name('update.title');
            Route::patch('/affairs/{affair}/stage', 'Api\Affair\AffairController@updateStage')->name('update.stage');
            Route::post('/affairs/sort', 'Api\Affair\AffairSortController@update')->name('sort');
            Route::delete('/affairs/{affair}', 'Api\Affair\AffairController@destroy')->name('destroy');
        });

        Route::group(['as' => 'projects.'], function () {
            Route::get('/projects', 'Api\Project\ProjectController@index')->name('index');
            Route::get('/projects/completed', 'Api\Project\CompletedProjectController@index')->name('completed');
            Route::post('/projects', 'Api\Project\ProjectController@store')->name('store');
            Route::patch('/projects/{project}/status', 'Api\Project\ProjectController@updateStatus')->name('update.status');
            Route::patch('/projects/{project}/title', 'Api\Project\ProjectController@updateTitle')->name('update.title');
            Route::post('/projects/sort', 'Api\Project\ProjectSortController@update')->name('sort');
            Route::delete('/projects/{project}', 'Api\Project\ProjectController@destroy')->name('destroy');
        });

        Route::group(['as' => 'journals.'], function () {
            Route::get('/journals', 'Api\Journal\JournalController@index')->name('index');
            Route::post('/journals', 'Api\Journal\JournalController@store')->name('store');
            Route::put('/journals/{journal}', 'Api\Journal\JournalController@update')->name('update');
            Route::delete('/journals/{journal}', 'Api\Journal\JournalController@destroy')->name('destroy');
        });
    });
});