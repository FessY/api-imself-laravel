<?php

namespace App\UseCases\Journal\DTO;

use Illuminate\Support\Collection;

class JournalCollection
{
    public $journals;
    public $dates;

    public function __construct(Collection $journals, array $dates)
    {
        $this->journals = $journals;
        $this->dates = $dates;
    }
}