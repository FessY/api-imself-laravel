<?php

namespace App\UseCases\Journal;

use App\Entities\SuccessJournal;
use App\UseCases\Journal\DTO\JournalCollection;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class JournalReadService
{
    public function index(string $userId): JournalCollection
    {
        $start = Carbon::today()->subDays(7);
        $end = Carbon::now();
        $interval = CarbonPeriod::between($start, $end)->toArray();

        $journals = SuccessJournal::forUser($userId)->days($start, $end)->get();

        $dates = collect($interval)->map(function (Carbon $date) {
            return $date->toDateString();
        })->toArray();

        rsort($dates);

        return new JournalCollection($journals, $dates);
    }
}