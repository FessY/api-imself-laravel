<?php

namespace App\UseCases\Journal;


use App\Entities\SuccessJournal;
use App\Entities\User\User;
use Illuminate\Database\DatabaseManager;

class JournalService
{
    private $db;

    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $userId
     * @param string $title
     * @return SuccessJournal
     * @throws \Throwable
     */
    public function create(string $userId, string $title)
    {
        $user = User::findOrFail($userId);

        $journal = SuccessJournal::new($user->id, $title);

        $this->db->transaction(function() use ($journal) {
            $journal->save();
        });

        return $journal;
    }

    /**
     * @param string $journalId
     * @param string $title
     * @throws \Throwable
     */
    public function edit(string $journalId, string $title)
    {
        $journal = SuccessJournal::findOrFail($journalId);

        $journal->edit($title);

        $this->db->transaction(function() use ($journal) {
            $journal->save();
        });
    }
}