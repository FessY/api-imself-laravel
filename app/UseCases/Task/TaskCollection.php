<?php

namespace App\UseCases\Task;

use Illuminate\Database\Eloquent\Collection;

class TaskCollection
{
    public $tasks;
    public $sort;

    public function __construct(Collection $tasks, array $sort)
    {
        $this->tasks = $tasks;
        $this->sort = $sort;
    }

    public function isEmptyTask(): bool
    {
        return $this->tasks->isEmpty();
    }
}