<?php

namespace App\UseCases\Task;

use App\Entities\Project\Task;
use App\Entities\Sort;
use App\Entities\User\User;
use Illuminate\Database\Eloquent\Collection;

class TaskReadService
{
    public function index(string $userId): TaskCollection
    {
        $user = User::findOrFail($userId);
        $tasks = Task::forUser($user->id)->active()->latest()->get();
        $sorts = Sort::forUser($user->id)->key(Task::sortKeyList())->get()
            ->flatMap(function ($item) {
                return [$item['key'] => $item['value']];
            })->toArray();

        $taskSorts = [
            Task::TYPE_TODAY => $this->getTypeOfSort(Task::SORT_KEY_TODAY, $sorts) ?? $this->getTypeOfTasks($tasks, function (Task $task) {
                    return $task->isToday();
                }),
            Task::TYPE_WEEK => $this->getTypeOfSort(Task::SORT_KEY_WEEK, $sorts) ?? $this->getTypeOfTasks($tasks, function (Task $task) {
                    return $task->isWeek();
                }),
            Task::TYPE_LATER => $this->getTypeOfSort(Task::SORT_KEY_LATER, $sorts) ?? $this->getTypeOfTasks($tasks, function (Task $task) {
                    return $task->isLater();
                }),
        ];

        return new TaskCollection($tasks, $taskSorts);
    }

    private function getTypeOfSort(string $type, array $sort)
    {
        return isset($sort[$type]) ? $sort[$type] : null;
    }

    private function getTypeOfTasks(Collection $tasks, callable $check)
    {
        return $tasks->filter(function (Task $task) use ($check) {
            return $check($task);
        })->pluck('id');
    }
}