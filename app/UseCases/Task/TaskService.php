<?php

namespace App\UseCases\Task;

use App\Entities\Project\Task;
use App\Entities\User\User;
use Illuminate\Database\DatabaseManager;

class TaskService
{
    private $db;

    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    /**
     * @return Task
     * @throws \Throwable
     */
    public function create(string $userId, string $title, string $type): Task
    {
        $user = User::findOrFail($userId);
        $task = Task::newWithoutProject($user->id, $title, $type);

        $this->db->transaction(function () use ($task) {
            $task->save();
        });

        return $task;
    }

    /**
     * @param string $id
     * @param string $title
     * @return Task
     * @throws \Throwable
     */
    public function editTitle(string $id, string $title): Task
    {
        $task = Task::findOrFail($id);

        $task->setTitle($title);

        $this->db->transaction(function () use ($task) {
            $task->save();
        });

        return $task;
    }

    /**
     * @param string $id
     * @param string $newType
     * @return TaskWithSortKeys
     * @throws \Throwable
     */
    public function changeType(string $id, string $newType): TaskWithSortKeys
    {
        $task = Task::findOrFail($id);
        $prevSortKey = $task->getSortKey();

        $task->changeType($newType);

        $this->db->transaction(function () use ($task) {
            $task->save();
        });

        return new TaskWithSortKeys($task, $task->getSortKey(), $prevSortKey);
    }
}