<?php

namespace App\UseCases\Task;

use App\Entities\Project\Task;

class TaskWithSortKeys
{
    public $task;
    public $currentSortKey;
    public $prevSortKey;

    public function __construct(Task $task, string $currentSortKey, string $prevSortKey)
    {
        $this->task = $task;
        $this->currentSortKey = $currentSortKey;
        $this->prevSortKey = $prevSortKey;
    }
}