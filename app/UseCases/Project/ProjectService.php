<?php

namespace App\UseCases\Project;


use App\Entities\Project\Project;
use App\Entities\User\User;
use Illuminate\Database\DatabaseManager;

class ProjectService
{
    private $db;

    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $userId
     * @param string $title
     * @return Project
     * @throws \Throwable
     */
    public function create(string $userId, string $title): Project
    {
        $user = User::findOrFail($userId);

        $project = Project::new($user->id, $title);

        $this->db->transaction(function () use ($project) {
            $project->save();
        });

        return $project;
    }

    /**
     * @param string $projectId
     * @param string $title
     * @throws \Throwable
     */
    public function editTitle(string $projectId, string $title)
    {
        $project = Project::findOrFail($projectId);

        $project->setTitle($title);

        $this->db->transaction(function () use ($project) {
            $project->save();
        });
    }

    /**
     * @param string $id
     * @param string $newStatus
     * @return ProjectWithSortKeys
     * @throws \Throwable
     */
    public function changeStatus(string $id, string $newStatus): ProjectWithSortKeys
    {
        $project = Project::findOrFail($id);
        $prevSortKey = $project->getSortKey();

        $project->changeStatus($newStatus);

        $this->db->transaction(function () use ($project) {
            $project->save();
        });

        return new ProjectWithSortKeys($project, $project->getSortKey(), $prevSortKey);
    }
}