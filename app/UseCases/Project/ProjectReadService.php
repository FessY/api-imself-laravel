<?php

namespace App\UseCases\Project;

use App\Entities\Project\Project;
use App\Entities\Sort;
use App\Entities\User\User;

class ProjectReadService
{
    public function index(string $userId): ProjectCollection
    {
        $user = User::findOrFail($userId);
        $projects = Project::forUser($user->id)->active()->latest()->get();
        $sort = Sort::forUser($user->id)->key(Project::SORT_KEY_ACTIVE)->first();

        return new ProjectCollection($projects, $sort);
    }
}