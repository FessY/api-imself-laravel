<?php

namespace App\UseCases\Project;

use App\Entities\Sort;
use Illuminate\Database\Eloquent\Collection;

class ProjectCollection
{
    public $projects;
    public $sort;

    public function __construct(Collection $projects, ?Sort $sort)
    {
        $this->projects = $projects;
        $this->sort = $sort;
    }
}