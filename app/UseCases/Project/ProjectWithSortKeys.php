<?php

namespace App\UseCases\Project;

use App\Entities\Project\Project;

class ProjectWithSortKeys
{
    public $project;
    public $currentSortKey;
    public $prevSortKey;

    public function __construct(Project $project, string $currentSortKey, string $prevSortKey)
    {
        $this->project = $project;
        $this->currentSortKey = $currentSortKey;
        $this->prevSortKey = $prevSortKey;
    }
}
