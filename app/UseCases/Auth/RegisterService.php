<?php

namespace App\UseCases\Auth;

use App\Entities\User\User;
use Illuminate\Database\DatabaseManager;

class RegisterService
{
    private $db;

    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @throws \Throwable
     */
    public function register(string $name, string $email, string $password)
    {
        $user = User::register($name, $email, $password);

        $this->db->transaction(function () use ($user) {
            $user->save();
        });
    }
}