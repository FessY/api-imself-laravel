<?php

namespace App\UseCases;

use App\Entities\Project\Task;
use App\Entities\Sort;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Request;

class SortService
{
    private $db;

    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $userId
     * @param string $sortKey
     * @param array $sortOrder
     * @return Sort
     * @throws \Throwable
     */
    public function createOrEdit(string $userId, string $sortKey, array $sortOrder): Sort
    {
        $sort = Sort::forUser($userId)->key($sortKey)->first();

        if (is_null($sort)) {
            $sort = Sort::new($userId, $sortKey, $sortOrder);
        } else {
            $sort->edit($sortKey, $sortOrder);
        }

        $this->db->transaction(function () use ($sort) {
            $sort->save();
        });

        return $sort;
    }

    /**
     * @param string $id
     * @param string $userId
     * @param string $sortKey
     * @throws \Throwable
     */
    public function addId(string $id, string $userId, string $sortKey)
    {
        $sort = Sort::forUser($userId)->where('key', $sortKey)->first();

        if (is_null($sort)) {
            return;
        }

        $sort->addId($id);

        $this->db->transaction(function () use ($sort) {
            $sort->save();
        });
    }

    /**
     * @param string $id
     * @param string $userId
     * @param string $currentSortKey
     * @param string $prevSortKey
     * @throws \Throwable
     */
    public function moveId(string $id, string $userId, string $currentSortKey, string $prevSortKey)
    {
        $sorts = Sort::forUser($userId)->key([$currentSortKey, $prevSortKey])->get();

        if (count($sorts) === 0) {
            return;
        }

        /** @var Sort $sort */
        foreach ($sorts as $sort) {
            if ($sort->isIdEqualTo($id)) {
                $sort->removeId($id);
                continue;
            }

            if ($sort->isKeyEqualTo($currentSortKey)) {
                $sort->addId($id);
            }
        }

        $this->db->transaction(function () use ($sorts) {
            /** @var Sort $sort */
            foreach ($sorts as $sort) {
                $sort->save();
            }
        });
    }

    public function removeId(string $id, string $userId, string $sortKey)
    {
        $sort = Sort::forUser($userId)->where('key', $sortKey)->first();

        if (is_null($sort)) {
            return;
        }

        $sort->removeId($id);

        $this->db->transaction(function () use ($sort) {
            $sort->save();
        });
    }
}