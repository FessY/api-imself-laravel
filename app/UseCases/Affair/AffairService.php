<?php

namespace App\UseCases\Affair;

use App\Entities\Affair;
use App\Entities\User\User;
use Illuminate\Database\DatabaseManager;

class AffairService
{
    private $db;

    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $userId
     * @param string $title
     * @param array $stages
     * @return Affair
     * @throws \Throwable
     */
    public function create(string $userId, string $title, array $stages)
    {
        $user = User::findOrFail($userId);
        $affair = Affair::new($user->id, $title, $stages);

        $this->db->transaction(function() use ($affair) {
            $affair->save();
        });

        return $affair;
    }

    /**
     * @param string $id
     * @param string $title
     * @throws \Throwable
     */
    public function editTitle(string $id, string $title)
    {
        $affair = Affair::findOrFail($id);

        $affair->setTitle($title);

        $this->db->transaction(function() use ($affair) {
            $affair->save();
        });
    }

    /**
     * @param string $id
     * @param string $date
     * @throws \Throwable
     */
    public function changeStage(string $id, string $date)
    {
        $affair = Affair::findOrFail($id);
        $date = Affair::parseDate($date);

        $affair->changeStage($date);

        $this->db->transaction(function() use ($affair) {
            $affair->save();
        });
    }
}