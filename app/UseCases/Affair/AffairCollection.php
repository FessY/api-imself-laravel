<?php

namespace App\UseCases\Affair;

use App\Entities\Sort;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class AffairCollection
{
    public $affairs;
    public $date;
    public $sort;

    public function __construct(Carbon $date, Collection $affairs, ?Sort $sort)
    {
        $this->date = $date;
        $this->affairs = $affairs;
        $this->sort = $sort;
    }
}