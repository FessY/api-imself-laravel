<?php

namespace App\Console\Commands\User;

use App\Entities\User\User;
use App\UseCases\Auth\RegisterService;
use Illuminate\Console\Command;

class RegUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:reg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register user';
    /**
     * @var RegisterService
     */
    private $service;

    /**
     * Create a new command instance.
     *
     * @param RegisterService $service
     */
    public function __construct(RegisterService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->ask('What is the name?');
        $email = $this->ask('What is the email?');
        $password = $this->secret('What is the password?');

        /** @var User $user */
        $user = User::where('email', $email)->first();

        if (!is_null($user)) {
            $this->error("Email - {$email} is exists.");
            return false;
        }

        try {
            $this->service->register($name, $email, $password);
        } catch (\Throwable $e) {
            $this->error($e->getMessage());
            return false;
        }

        $this->info('User is successfully register.');
        return true;
    }
}
