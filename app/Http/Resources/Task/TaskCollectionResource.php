<?php

namespace App\Http\Resources\Task;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskCollectionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        /** @var \App\UseCases\Task\TaskCollection $collection */
        $collection = $this->resource;
        $tasks = $collection->tasks;
        $taskIds = $collection->sort;

        return [
            'tasks' => $tasks->keyBy('id'),
            'taskIds' => $taskIds
        ];
    }
}
