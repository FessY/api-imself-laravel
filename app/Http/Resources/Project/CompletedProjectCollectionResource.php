<?php

namespace App\Http\Resources\Project;

use App\Entities\Project\Project;
use App\Http\Resources\Task\TaskResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class CompletedProjectCollectionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Collection $collection */
        $projects = $this->resource;
        $tasks = $projects->flatMap(function (Project $project) {
            return $project->tasks;
        });

        /** @var Collection $projectCollection */
        $projectCollection = ProjectResource::collection($projects);
        /** @var Collection $taskCollection */
        $taskCollection = TaskResource::collection($tasks);

        return [
            'project' => $projectCollection->keyBy('id'),
            'projectIds' => $projectCollection->pluck('id'),
            'tasks' => $taskCollection->keyBy('id'),
        ];
    }
}
