<?php

namespace App\Http\Resources\Project;

use App\Entities\Project\Project;
use App\Http\Resources\Task\TaskResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class ProjectCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection;
        $tasks = $collection->flatMap(function (Project $project) {
            return $project->tasks;
        });

        /** @var Collection $projectCollection */
        $projectCollection = ProjectResource::collection($collection);
        /** @var Collection $taskCollection */
        $taskCollection = TaskResource::collection($tasks);

        return [
            'projects' => $projectCollection->keyBy('id'),
            'tasks' => $taskCollection->keyBy('id'),
            'projectIds' => $projectCollection->pluck('id'),
        ];
    }
}
