<?php

namespace App\Http\Resources\Project;

use App\Entities\Project\Project;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Project $project */
        $project = $this->resource;
        $task = $project->tasks;
        $taskSort = $project->taskSort;

        return [
            'id' => $project->id,
            'title' => $project->title,
            'status' => $project->status,
            'created_at' => $project->created_at->toDateTimeString(),
            'updated_at' => $project->updated_at->toDateTimeString(),
            'taskIds' => !is_null($taskSort) ? $taskSort->sort : $task->pluck('id')
        ];
    }
}
