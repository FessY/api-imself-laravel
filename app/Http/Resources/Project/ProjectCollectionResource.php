<?php

namespace App\Http\Resources\Project;

use App\Entities\Project\Project;
use App\Http\Resources\Task\TaskResource;
use App\UseCases\Project\ProjectCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class ProjectCollectionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var ProjectCollection $collection */
        $collection = $this->resource;
        $projects = $collection->projects;
        $sort = $collection->sort;
        $tasks = $projects->flatMap(function (Project $project) {
            return $project->tasks;
        });

        /** @var Collection $projectCollection */
        $projectCollection = ProjectResource::collection($projects);
        /** @var Collection $taskCollection */
        $taskCollection = TaskResource::collection($tasks);

        return [
            'projects' => $projectCollection->keyBy('id'),
            'projectIds' => !is_null($sort) ? $sort->value : $projectCollection->pluck('id'),
            'tasks' => $taskCollection->keyBy('id'),
        ];
    }
}
