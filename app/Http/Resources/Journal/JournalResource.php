<?php

namespace App\Http\Resources\Journal;

use App\Entities\SuccessJournal;
use Illuminate\Http\Resources\Json\JsonResource;

class JournalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var SuccessJournal $journal */
        $journal = $this->resource;

        return [
            'id' => $journal->id,
            'title' => $journal->title,
            'date' => $journal->created_at->toDateString(),
        ];
    }
}
