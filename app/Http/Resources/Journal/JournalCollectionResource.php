<?php

namespace App\Http\Resources\Journal;

use App\Entities\SuccessJournal;
use App\UseCases\Journal\DTO\JournalCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class JournalCollectionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var JournalCollection $collection */
        $collection = $this->resource;
        $journals = $collection->journals;
        /** @var Collection $journalsCollection */
        $journalsCollection = JournalResource::collection($journals);
        $journalIds = $journals->mapToGroups(function (SuccessJournal $journal) {
            return [$journal->created_at->toDateString() => $journal->id];
        });
        $dates = $collection->dates;

        return [
            'journals' => $journalsCollection->keyBy('id'),
            'journalIds' => $journalIds,
            'dates' => $dates,
        ];
    }
}
