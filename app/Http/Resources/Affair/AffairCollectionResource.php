<?php

namespace App\Http\Resources\Affair;

use App\Entities\Affair;
use Illuminate\Http\Resources\Json\JsonResource;

class AffairCollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\UseCases\Affair\AffairCollection $collection */
        $collection = $this->resource;
        $affairs = $collection->affairs;
        $sort = $collection->sort;
        $date = $collection->date;

        return [
            'date' => Affair::formatDate($date),
            'affairs' => $affairs->keyBy('id'),
            'affairIds' => !is_null($sort) ? $sort->value : $affairs->pluck('id'),
        ];
    }
}
