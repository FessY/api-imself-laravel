<?php

namespace App\Http\Resources\Affair;

use App\Entities\Affair;
use Illuminate\Http\Resources\Json\JsonResource;

class AffairResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Affair $affair */
        $affair = $this->resource;

        return [
            'affair' => [
                'id' => $affair->id,
                'title' => $affair->title,
                'days' => $affair->days,
                'status' => $affair->status,
                'created_at' => $affair->created_at->toDateTimeString(),
                'updated_at' => $affair->updated_at->toDateTimeString(),
            ]
        ];
    }
}
