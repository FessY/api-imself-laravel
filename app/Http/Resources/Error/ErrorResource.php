<?php

namespace App\Http\Resources\Error;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class ErrorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $text = $this->resource;

        return [
            'message' => $text,
            'errors' => (object) [],
        ];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(Response::HTTP_BAD_REQUEST);
    }
}
