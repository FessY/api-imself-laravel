<?php

namespace App\Http\Controllers\Api\Task;

use App\Entities\Project\Task;
use App\Http\Resources\Task\TaskCollectionResource;
use App\Http\Resources\Task\TaskResource;
use App\UseCases\SortService;
use App\UseCases\Task\TaskReadService;
use App\UseCases\Task\TaskService;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    private $readService;
    private $service;
    private $sortService;

    public function __construct(
        TaskReadService $readService,
        TaskService $service,
        SortService $sortService
    )
    {
        $this->readService = $readService;
        $this->service = $service;
        $this->sortService = $sortService;
    }

    public function index()
    {
        $user = Auth::user();
        $collection = $this->readService->index($user->id);
        return new TaskCollectionResource($collection);
    }

    /**
     * @param Request $request
     * @return TaskResource
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $task = $this->service->create($user->id, $request->json('title'), $request->json('type'));
        $this->sortService->addId($task->id, $user->id, $task->getSortKey());
        return new TaskResource($task);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function updateTitle(Request $request, string $id)
    {
        $task = $this->service->editTitle($id, $request->json('title'));
        $this->sortService->moveId($task->id, Auth::id(), $task->getSortKey(), $request->json('type'));
        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return TaskResource
     * @throws \Throwable
     */
    public function updateType(Request $request, string $id)
    {
        $taskWithSortKeys = $this->service->changeType($id, $request->json('newType'));
        $task = $taskWithSortKeys->task;

        $this->sortService->moveId(
            $task->id,
            Auth::id(),
            $taskWithSortKeys->currentSortKey,
            $taskWithSortKeys->prevSortKey
        );
        return new TaskResource($task);
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(string $id)
    {
        $task = Task::findOrFail($id);
        $this->sortService->removeId($task->id, Auth::id(), $task->getSortKey());
        $task->delete();
        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
