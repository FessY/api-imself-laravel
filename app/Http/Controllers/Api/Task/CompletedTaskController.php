<?php

namespace App\Http\Controllers\Api\Task;

use App\Entities\Project\Task;
use App\Http\Controllers\Controller;
use App\Http\Resources\Task\CompletedTaskCollection;
use Auth;

class CompletedTaskController extends Controller
{
    public function index()
    {
        $tasks = Task::forUser(Auth::id())->completed()->latest('updated_at')->paginate();
        return new CompletedTaskCollection($tasks);
    }
}
