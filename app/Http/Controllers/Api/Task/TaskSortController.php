<?php

namespace App\Http\Controllers\Api\Task;

use App\UseCases\SortService;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class TaskSortController extends Controller
{
    private $sortService;

    public function __construct(SortService $sortService)
    {
        $this->sortService = $sortService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(Request $request)
    {
        //$task = Task::findOrFail($request->json('id'));

        $this->sortService->createOrEdit(
            Auth::id(),
            $request->json('sortKey'),
            $request->json('sortOrder')
        );

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
