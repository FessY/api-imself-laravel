<?php

namespace App\Http\Controllers\Api\Auth;

use App\Entities\User\User;
use App\Exceptions\NoUserException;
use App\Http\Controllers\Controller;
use App\Http\Resources\Error\ErrorResource;
use App\Http\Resources\TokenResource;
use App\Services\Auth\JWTAuth;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    private $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * @param Request $request
     * @return ErrorResource|TokenResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);

        try {
            $user = $this->getUser($request->input('email'));
        } catch (NoUserException $e) {
            return new ErrorResource($e->getMessage());
        }

        if (app('hash')->check($request->input('password'), $user->password)) {
            return new TokenResource($this->jwt->create($user));
        }

        return new ErrorResource('Email or password is wrong.');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string|max:255'
        ]);

        Cache::forever($request->json('token'), null);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param string $email
     * @return User|\Illuminate\Database\Eloquent\Model
     * @throws NoUserException
     */
    protected function getUser(string $email)
    {
        $user = User::where('email', $email)->first();

        if (!$user) throw new NoUserException('No such user');

        return $user;
    }
}
