<?php

namespace App\Http\Controllers\Api\Journal;

use App\Entities\SuccessJournal;
use App\Http\Resources\Journal\JournalCollectionResource;
use App\Http\Resources\Journal\JournalResource;
use App\UseCases\Journal\JournalReadService;
use App\UseCases\Journal\JournalService;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class JournalController extends Controller
{
    private $service;
    private $readService;

    public function __construct(JournalReadService $readService, JournalService $service)
    {
        $this->service = $service;
        $this->readService = $readService;
    }

    public function index()
    {
        $journalCollection = $this->readService->index(Auth::id());
        return new JournalCollectionResource($journalCollection);
    }

    /**
     * @param Request $request
     * @return JournalResource
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $journal = $this->service->create(Auth::id(), $request->json('title'));
        return new JournalResource($journal);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(Request $request, string $id)
    {
        $this->service->edit($id, $request->json('title'));
        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(string $id)
    {
        $journal = SuccessJournal::findOrFail($id);
        $journal->delete();
        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
