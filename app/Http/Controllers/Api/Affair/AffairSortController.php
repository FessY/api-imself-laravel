<?php

namespace App\Http\Controllers\Api\Affair;

use App\Entities\Affair;
use App\UseCases\SortService;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class AffairSortController extends Controller
{
    private $sortService;

    public function __construct(SortService $sortService)
    {
        $this->sortService = $sortService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(Request $request)
    {
        $this->sortService->createOrEdit(
            Auth::id(),
            Affair::SORT_KEY,
            $request->json('sortOrder')
        );

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
