<?php

namespace App\Http\Controllers\Api\Affair;

use App\Entities\Affair;
use App\Entities\Sort;
use App\Http\Resources\Affair\AffairCollectionResource;
use App\Http\Resources\Affair\AffairResource;
use App\UseCases\Affair\AffairCollection;
use App\UseCases\Affair\AffairService;
use App\UseCases\SortService;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class AffairController extends Controller
{
    private $service;
    private $sortService;

    public function __construct(AffairService $service, SortService $sortService)
    {
        $this->service = $service;
        $this->sortService = $sortService;
    }

    public function index(string $date = null)
    {
        $date = Affair::parseDate($date);
        $user = Auth::user();

        $affairs = Affair::forUser($user->id)->latest()->get();
        $sort = Sort::forUser($user->id)->key(Affair::SORT_KEY)->first();

        return new AffairCollectionResource(new AffairCollection($date, $affairs, $sort));
    }

    /**
     * @param Request $request
     * @return AffairResource
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $affair = $this->service->create($user->id, $request->json('title'), []);
        $this->sortService->addId($affair->id, $user->id, $affair->getSortKey());

        return new AffairResource($affair);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function updateTitle(Request $request, string $id)
    {
        $this->service->editTitle($id, $request->json('title'));
        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function updateStage(Request $request, string $id)
    {
        $this->service->changeStage($id, $request->json('date'));
        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(string $id)
    {
        $affair = Affair::findOrFail($id);
        $this->sortService->removeId($affair->id, Auth::id(), $affair->getSortKey());
        $affair->delete();

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
