<?php

namespace App\Http\Controllers\Api\Project;

use App\Entities\Project\Project;
use App\Http\Resources\Project\ProjectCollection;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompletedProjectController extends Controller
{
    public function index()
    {
        $projects = Project::forUser(Auth::id())->completed()->latest('updated_at')->paginate();
        return new ProjectCollection($projects);
    }
}
