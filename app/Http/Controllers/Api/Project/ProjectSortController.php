<?php

namespace App\Http\Controllers\Api\Project;

use App\UseCases\SortService;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ProjectSortController extends Controller
{
    private $sortService;

    public function __construct(SortService $sortService)
    {
        $this->sortService = $sortService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(Request $request)
    {
        //$project = Project::findOrFail($request->json('id'));

        $this->sortService->createOrEdit(
            Auth::id(),
            $request->json('keySort'),
            $request->json('sortOrder'),
            );

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
