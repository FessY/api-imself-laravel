<?php

namespace App\Http\Controllers\Api\Project;

use App\Entities\Project\Project;
use App\Http\Resources\Project\ProjectCollectionResource;
use App\Http\Resources\Project\ProjectResource;
use App\UseCases\Project\ProjectReadService;
use App\UseCases\Project\ProjectService;
use App\UseCases\SortService;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ProjectController extends Controller
{
    private $readService;
    private $service;
    private $sortService;

    public function __construct(ProjectReadService $readService, ProjectService $service, SortService $sortService)
    {
        $this->readService = $readService;
        $this->service = $service;
        $this->sortService = $sortService;
    }

    public function index()
    {
        $projectCollection = $this->readService->index(Auth::id());
        return new ProjectCollectionResource($projectCollection);
    }

    /**
     * @param Request $request
     * @return ProjectResource
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $project = $this->service->create($user->id, $request->json('title'));
        $this->sortService->addId($project->id, $user->id, $project->getSortKey());
        return new ProjectResource($project);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function updateTitle(Request $request, string $id)
    {
        $this->service->editTitle($id, $request->json('title'));
        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return ProjectResource
     * @throws \Throwable
     */
    public function updateStatus(Request $request, string $id)
    {
        $projectWithSortKeys = $this->service->changeStatus($id, $request->json('newStatus'));
        $project = $projectWithSortKeys->project;

        $this->sortService->moveId(
            $project->id,
            Auth::id(),
            $projectWithSortKeys->currentSortKey,
            $projectWithSortKeys->prevSortKey
        );

        return new ProjectResource($project);
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(string $id)
    {
        $project = Project::findOrFail($id);
        $this->sortService->removeId($project->id, Auth::id(), $project->getSortKey());
        $project->delete();
        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
