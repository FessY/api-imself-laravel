<?php

namespace App\Http\Middleware;


use App\Services\Auth\JWTAuth;
use Auth;
use Closure;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Response;

class JWT
{
    private $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->hasHeader('authorization')) {
            return response()->json(['errors' => 'Token not provided.'], Response::HTTP_BAD_REQUEST);
        }

        $token = $request->header('authorization');

        try {
            $credentials = $this->jwt->decode($token);
        } catch (ExpiredException $e) {
            return response()->json(['errors' => 'Token is expired.'], Response::HTTP_UNAUTHORIZED);
        } catch (\Exception $e) {
            return response()->json(['errors' => "An error while decoding token. {$e->getMessage()}"], Response::HTTP_BAD_REQUEST);
        }

        Auth::onceUsingId($credentials->sub);

        return Auth::check() ? $next($request): abort(Response::HTTP_UNAUTHORIZED);
    }
}
