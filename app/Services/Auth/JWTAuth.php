<?php

namespace App\Services\Auth;

use App\Entities\User\User;
use Cache;
use Carbon\Carbon;
use Firebase\JWT\JWT;

class JWTAuth
{
    public function create(User $user)
    {
        $time = Carbon::now();

        $payload = [
            'iss' => 'jwt',
            'sub' => $user->id,
            'iat' => $time->timestamp,
            'exp' => $time->addMinutes(config('jwt.ttl'))->timestamp,
        ];

        return JWT::encode($payload, config('jwt.secret'), 'HS256');
    }

    /**
     * @param $token
     * @return object
     * @throws \Exception
     */
    public function decode(string $token)
    {
        if ($this->checkInBlackList($token)) {
            throw new \Exception('Token is blocked');
        }

        return JWT::decode($token, config('jwt.secret'), ['HS256']);
    }

    private function checkInBlackList(string $token): bool
    {
        return Cache::has($token);
    }
}