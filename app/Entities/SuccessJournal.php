<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static SuccessJournal forUser($userId)
 * @method static SuccessJournal days($start, $end)
 *
 * @mixin \Eloquent
 */
class SuccessJournal extends Model
{
    protected $table = 'success_journals';

    public $incrementing = false;

    public static function new(string $userId, string $title): SuccessJournal
    {
        $journal = new static();

        $journal->id = Str::uuid()->toString();
        $journal->user_id = $userId;
        $journal->title = $title;

        return $journal;
    }

    public function edit(string $title)
    {
        $this->title = $title;
    }

    public function scopeForUser(Builder $query, string $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function scopeDays(Builder $query, string $start, string $end)
    {
        return $query->whereBetween('created_at', [$start, $end]);
    }
}
