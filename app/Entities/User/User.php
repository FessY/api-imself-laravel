<?php

namespace App\Entities\User;

use App\Entities\Project\Task;
use Illuminate\Notifications\Notifiable;
//use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

/**
 * App\Entities\User\User
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property \Illuminate\Database\Eloquent\Collection|\App\Entities\Project\Task[] $tasks
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public $incrementing = false;

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     * @throws \Exception
     */
    public static function register(string $name, string $email, string $password): self
    {
        $user = new static();
        $user->id = Str::uuid()->toString();
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);

        return $user;
    }
}
