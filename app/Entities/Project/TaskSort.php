<?php

namespace App\Entities\Project;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Project\TaskSort
 *
 * @property string $project_id
 * @property array $sort
 * @property string $status
 *
 * @mixin \Eloquent
 */
class TaskSort extends Model
{
    protected $table = 'projects_task_sorts';

    public $incrementing = false;
    public $timestamps = false;

    protected $casts = [
        'sort' => 'array',
    ];

    public static function new(string $projectId, array $taskSort): self
    {
        $sort = new static();
        $sort->project_id = $projectId;
        $sort->sort = $taskSort;

        return $sort;
    }

    public function editSort(array $taskSort)
    {
        $this->sort = $taskSort;
    }
}
