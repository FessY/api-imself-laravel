<?php

namespace App\Entities\Project;

use App\Entities\User\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * App\Entities\Project\Project
 *
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Database\Eloquent\Collection|\App\Entities\Project\Task[] $tasks
 * @property \App\Entities\Project\TaskSort $taskSort
 * @property \App\Entities\User\User $user
 *
 * @method static Project active()
 * @method static Project completed()
 * @method static Project forUser(string $userId)
 *
 * @mixin \Eloquent
 */
class Project extends Model
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_COMPLETED = 'completed';

    public const SORT_KEY_ACTIVE = 'active-projects';
    public const SORT_KEY_COMPLETED = 'completed-projects';

    protected $table = 'projects';

    public $incrementing = false;

    protected $with = ['tasks', 'taskSort'];

    public static function new(string $userId, string $title): self
    {
        $project = new static();
        $project->id = Str::uuid()->toString();
        $project->user_id = $userId;
        $project->title = $title;
        $project->status = self::STATUS_ACTIVE;

        return $project;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function changeStatus(string $status)
    {
        $this->status = $status;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isCompleted(): bool
    {
        return $this->status === self::STATUS_COMPLETED;
    }

    public function addTask(string $title, string $type = null)
    {
        $tasks = $this->tasks;
        $tasks[] = Task::new($this->id, $this->user_id, $title, $type);
        $this->updateTasks($tasks);
    }

    public function editTask(string $taskId, string $title, string $type = null)
    {
        $tasks = $this->tasks;

        foreach ($tasks as $task) {
            if ($task->isIdEqualTo($taskId)) {
                $task->edit($title, $type);
                $this->updateTasks($tasks);
                return;
            }
        }
    }

    public function changeStatusTask(string $taskId, string $taskType)
    {
        $tasks = $this->tasks;

        foreach ($tasks as $task) {
            if ($task->isIdEqualTo($taskId)) {
                $task->changeType($taskType);
                $this->updateTasks($tasks);
                return;
            }
        }
    }

    public function removeTask(string $taskId)
    {
        $tasks = $this->tasks;

        foreach ($tasks as $i => $task) {
            if ($task->isIdEqualTo($taskId)) {
                unset($tasks[$i]);
                $this->updateTasks($tasks);
                return;
            }
        }
    }

    private function updateTasks(Collection $tasks)
    {
        $this->setRelation('tasks', $tasks);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'project_id');
    }

    public function taskSort()
    {
        return $this->hasOne(TaskSort::class, 'project_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getSortKey()
    {
        return $this->isActive() ? self::SORT_KEY_ACTIVE : self::SORT_KEY_COMPLETED;
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeCompleted(Builder $builder)
    {
        return $builder->where('status', self::STATUS_COMPLETED);
    }

    public function scopeForUser(Builder $query, string $userId)
    {
        return $query->where('user_id', $userId);
    }
}
