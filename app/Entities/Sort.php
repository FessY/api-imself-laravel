<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @property string $id
 * @property string $user_id
 * @property string $key
 * @property array $value
 *
 * @method static Sort forUser(string $userId)
 * @method static Sort key(array|string $sortKey)
 *
 * @mixin \Eloquent
 */
class Sort extends Model
{
    protected $table = 'sorts';

    public $incrementing = false;

    public $timestamps = false;

    protected $casts = [
        'value' => 'array',
    ];

    public static function new(string $userId, string $key, array $value): self
    {
        $sort = new static();
        $sort->id = Str::uuid()->toString();
        $sort->user_id = $userId;
        $sort->key = $key;
        $sort->value = $value;

        return $sort;
    }

    public function edit(string $key, array $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function addId(string $id)
    {
        $arr = $this->value;
        array_unshift($arr, $id);
        $this->value = $arr;
    }

    public function isIdEqualTo(string $id): bool
    {
        return in_array($id, $this->value);
    }

    public function removeId(string $id)
    {
        $arr = $this->value;
        $pos = array_search($id, $arr);

        if ($pos !== false) {
            array_splice($arr, $pos, 1);
        }

        $this->value = $arr;
    }

    public function isKeyEqualTo(string $key): bool
    {
        return $this->key === $key;
    }

    public function scopeForUser(Builder $query, string $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function scopeKey(Builder $query, $sortKey)
    {
        if (is_array($sortKey)) {
            return $query->whereIn('key', $sortKey);
        }

        return $query->where('key', $sortKey);
    }
}
