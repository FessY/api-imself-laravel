<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Affair
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property array $stages
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \App\Entities\User\User $user
 *
 * @method static Affair forUser(string $userId)
 *
 * @mixin \Eloquent
 */
class Affair extends Model
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_COMPLETED = 'completed';

    public const SORT_KEY = 'affairs';

    protected $table = 'affairs';

    public $incrementing = false;

    protected $casts = [
        'stages' => 'array',
    ];

    public static function parseDate(string $date = null): Carbon
    {
        return is_null($date) ? Carbon::today() : Carbon::parse($date);
    }

    public function getSortKey()
    {
        return self::SORT_KEY;
    }

    public static function formatDate(Carbon $date)
    {
        return $date->format('Y-m-d');
    }

    public static function new(string $userId, string $title, array $stages): self
    {
        $affair = new static();
        $affair->id = Str::uuid()->toString();
        $affair->user_id = $userId;
        $affair->title = $title;
        $affair->stages = $stages;
        $affair->status = self::STATUS_ACTIVE;

        return $affair;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function changeStage(Carbon $date)
    {
        $date = self::formatDate($date);
        $stages = $this->stages;

        if (!isset($stages[$date])) {
            $stages[$date] = true;
        } else {
            unset($stages[$date]);
        }

        ksort($stages);

        $this->stages = $stages;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isCompleted(): bool
    {
        return $this->status === self::STATUS_COMPLETED;
    }

    public function scopeForUser(Builder $query, string $userId)
    {
        return $query->where('user_id', $userId);
    }
}
