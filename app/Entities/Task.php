<?php

namespace App\Entities\Project;

use App\Entities\User\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Entities\Project\Task
 *
 * @property string $id
 * @property string $user_id
 * @property string|null $project_id
 * @property string $title
 * @property string $type
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \App\Entities\User\User $user
 *
 * @method static Task active()
 * @method static Task completed()
 * @method static Task forUser(string $userId)
 *
 * @mixin \Eloquent
 */
class Task extends Model
{
    public const TYPE_TODAY = 'today';
    public const TYPE_WEEK = 'week';
    public const TYPE_LATER = 'later';
    public const TYPE_COMPLETED = 'completed';

    public const SORT_KEY_TODAY = 'today-tasks';
    public const SORT_KEY_WEEK = 'week-tasks';
    public const SORT_KEY_LATER = 'later-tasks';

    public static function typeList(): array
    {
        return [
            self::TYPE_TODAY => 'Today',
            self::TYPE_WEEK => 'Week',
            self::TYPE_LATER => 'Later',
            self::TYPE_COMPLETED => 'Completed'
        ];
    }

    public static function sortKeyList()
    {
        return [
            self::SORT_KEY_TODAY,
            self::SORT_KEY_WEEK,
            self::SORT_KEY_LATER,
        ];
    }

    protected $table = 'tasks';

    public $incrementing = false;

    public static function new(string $projectId, string $userId, string $title, string $type = null): self
    {
        $task = new static();
        $task->id = Str::uuid()->toString();
        $task->project_id = $projectId;
        $task->user_id = $userId;
        $task->title = $title;
        $task->type = $type ?? self::TYPE_TODAY;

        return $task;
    }

    public static function newWithoutProject(string $userId, string $title, string $type = null): self
    {
        $task = new static();
        $task->id = Str::uuid()->toString();
        $task->user_id = $userId;
        $task->project_id = null;
        $task->title = $title;
        $task->type = $type ?? self::TYPE_TODAY;

        return $task;
    }

    public function complete()
    {
        $this->type = self::TYPE_COMPLETED;
    }

    public function activate()
    {
        $this->type = self::TYPE_TODAY;
    }

    public function changeType(string $type)
    {
        $this->type = $type;
    }

    public function isIdEqualTo(string $id): bool
    {
        return $this->id === $id;
    }

    public function isActive(): bool
    {
        return $this->type !== self::TYPE_COMPLETED;
    }

    public function isCompleted(): bool
    {
        return $this->type === self::TYPE_COMPLETED;
    }

    public function isToday(): bool
    {
        return $this->type === self::TYPE_TODAY;
    }

    public function isWeek(): bool
    {
        return $this->type === self::TYPE_WEEK;
    }

    public function isLater(): bool
    {
        return $this->type === self::TYPE_LATER;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function edit(string $title, ?string $type)
    {
        $this->setTitle($title);
        $this->type = $type ?? $this->type;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function assignProject(string $projectId)
    {
        $this->project_id = $projectId;
    }

    public function getSortKey()
    {
        if ($this->isWeek()) {
            return self::SORT_KEY_WEEK;
        }

        if ($this->isLater()) {
            return self::SORT_KEY_LATER;
        }

        if ($this->isToday()) {
            return self::SORT_KEY_TODAY;
        }

        return self::TYPE_COMPLETED;
    }

    public function scopeActive(Builder $builder)
    {
        return $builder->where('type', '!=', self::TYPE_COMPLETED);
    }

    public function scopeCompleted(Builder $builder)
    {
        return $builder->where('type', self::TYPE_COMPLETED);
    }

    public function scopeForUser(Builder $query, string $userId)
    {
        return $query->where('user_id', $userId);
    }
}
