<?php

use App\Entities\SuccessJournal;
use App\Entities\User\User;
use Faker\Generator as Faker;

/* @var $factory \Illuminate\Database\Eloquent\Factory */
$factory->define(SuccessJournal::class, function (Faker $faker) {
    return [
        'id' => $faker->uuid,
        'user_id' => function () {
            return create(User::class)->id;
        },
        'title' => $faker->text(100),
    ];
});
