<?php

use App\Entities\Affair;
use App\Entities\User\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

/* @var $factory \Illuminate\Database\Eloquent\Factory */
$factory->define(Affair::class, function (Faker $faker) {
    $date = Carbon::today();
    $active = $faker->boolean;
    $stages = [];

    for ($i = 1; $i <= $faker->numberBetween(3, 20); $i++) {
        $stages[$date->subDays($i)->toDateString()] = true;
    }

    return [
        'id' => $faker->uuid,
        'user_id' => function () {
            return create(User::class)->id;
        },
        'title' => $faker->text(100),
        'stages' => $stages,
        'status' => $active ? Affair::STATUS_ACTIVE : Affair::STATUS_COMPLETED,
    ];
});
