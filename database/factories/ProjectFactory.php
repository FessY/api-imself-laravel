<?php

use App\Entities\Project\Project;
use App\Entities\User\User;
use Faker\Generator as Faker;

/* @var $factory \Illuminate\Database\Eloquent\Factory */
$factory->define(Project::class, function (Faker $faker) {
    $active = $faker->boolean;

    return [
        'id' => $faker->uuid,
        'user_id' => function () {
            return create(User::class)->id;
        },
        'title' => $faker->text(100),
        'status' => $active ? Project::STATUS_ACTIVE : Project::STATUS_COMPLETED,
    ];
});
