<?php

use App\Entities\Sort;
use App\Entities\User\User;
use Faker\Generator as Faker;

/* @var $factory \Illuminate\Database\Eloquent\Factory */
$factory->define(Sort::class, function (Faker $faker) {
    return [
        'id' => $faker->uuid,
        'user_id' => function () {
            return create(User::class)->id;
        },
        'key' => $faker->unique()->text(10),
        'value' => [$faker->uuid, $faker->uuid, $faker->uuid],
    ];
});
