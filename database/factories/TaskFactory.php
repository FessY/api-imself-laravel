<?php

use App\Entities\Project\Project;
use App\Entities\Project\Task;
use App\Entities\User\User;
use Faker\Generator as Faker;

/* @var $factory \Illuminate\Database\Eloquent\Factory */
$factory->define(Task::class, function (Faker $faker) {
    return [
        'id' => $faker->uuid,
        'user_id' => function () {
            return create(User::class)->id;
        },
        'project_id' => function () {
            return create(Project::class)->id;
        },
        'title' => $faker->text(100),
        'type' => $faker->randomKey(Task::typeList()),
    ];
});
