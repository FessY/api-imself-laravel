<?php

use App\Entities\Sort;
use App\Entities\User\User;
use Illuminate\Database\Seeder;

class SortTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        factory(Sort::class, 3)->create(['user_id' => $user->id]);
    }
}
