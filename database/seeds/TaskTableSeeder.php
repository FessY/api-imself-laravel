<?php

use App\Entities\Project\Task;
use App\Entities\User\User;
use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        factory(Task::class, 3)->create(['user_id' => $user->id]);
        factory(Task::class, 3)->create(['user_id' => $user->id, 'project_id' => null]);
    }
}
