<?php

use App\Entities\User\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create(['name' => 'Test', 'email' => 'test@site.local']);
        factory(User::class)->create(['name' => 'Test 2', 'email' => 'test2@site.local']);
    }
}
