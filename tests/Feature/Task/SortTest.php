<?php

namespace Tests\Unit\Task;

use App\Entities\Project\Task;
use App\Entities\Sort;
use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SortTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    public function testMoveId()
    {
        $this->signIn();
        /** @var Task $task */
        $task = create(Task::class, ['type' => Task::TYPE_TODAY, 'user_id' => auth()->id()]);
        /** @var Sort $sortToday */
        $sortToday = create(Sort::class, ['user_id' => auth()->id(), 'key' => Task::SORT_KEY_TODAY, 'value' => [$task->id]]);
        /** @var Sort $sortWeek */
        $sortWeek = create(Sort::class, ['user_id' => auth()->id(), 'key' => Task::SORT_KEY_WEEK]);

        $this->patchJson(route('api.tasks.update.type', $task), ['newType' => Task::TYPE_WEEK])->json();

        $this->assertFalse(in_array($task->id, $sortToday->fresh()->value));
        $this->assertTrue(in_array($task->id, $sortWeek->fresh()->value));

        $this->patchJson(route('api.tasks.update.type', $task), ['newType' => Task::TYPE_TODAY])->json();

        $this->assertTrue(in_array($task->id, $sortToday->fresh()->value));
        $this->assertFalse(in_array($task->id, $sortWeek->fresh()->value));
    }

    public function testMoveIdToCompleted()
    {
        $this->signIn();
        /** @var Task $task */
        $task = create(Task::class, ['type' => Task::TYPE_TODAY, 'user_id' => auth()->id()]);
        /** @var Sort $sortToday */
        $sortToday = create(Sort::class, ['user_id' => auth()->id(), 'key' => Task::SORT_KEY_TODAY, 'value' => [$task->id]]);

        $this->patchJson(route('api.tasks.update.type', $task), ['newType' => Task::TYPE_COMPLETED])->json();

        $this->assertFalse(in_array($task->id, $sortToday->fresh()->value));
    }

    public function testMoveIdFromCompleted()
    {
        $this->signIn();
        /** @var Task $task */
        $task = create(Task::class, ['type' => Task::TYPE_COMPLETED, 'user_id' => auth()->id()]);
        /** @var Sort $sortToday */
        $sortToday = create(Sort::class, ['user_id' => auth()->id(), 'key' => Task::SORT_KEY_TODAY]);

        $this->assertFalse(in_array($task->id, $sortToday->value));

        $this->patchJson(route('api.tasks.update.type', $task), ['newType' => Task::TYPE_TODAY])->json();

        $this->assertTrue(in_array($task->id, $sortToday->fresh()->value));
    }
}
