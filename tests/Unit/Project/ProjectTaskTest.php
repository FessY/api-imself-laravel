<?php

namespace Tests\Unit\Project;

use App\Entities\Project\Project;
use App\Entities\Project\Task;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTaskTest extends TestCase
{
    use RefreshDatabase;

    public function testAddTask()
    {
        /** @var Project $project */
        $project = make(Project::class);

        $this->assertCount(0, $project->tasks);

        $project->addTask('Test task');
        $project->addTask('Test task 2');

        $this->assertCount(2, $project->tasks);
    }

    public function testEditTask()
    {
        /** @var Project $project */
        $project = make(Project::class);
        $project->addTask('Test task');
        $task = $project->tasks[0];

        $project->editTask($task->id, $title = 'Test task');

        $this->assertEquals($title, $task->title);
        $this->assertTrue($task->isToday());

        $project->editTask($task->id, $title = 'Test task 2', Task::TYPE_WEEK);

        $this->assertEquals($title, $task->title);
        $this->assertTrue($task->isWeek());
    }

    public function testChangeStatusTask()
    {
        /** @var Project $project */
        $project = make(Project::class);
        $project->addTask('Test task');
        $task = $project->tasks[0];

        $this->assertTrue($task->isActive());

        $project->changeStatusTask($task->id, Task::TYPE_COMPLETED);

        $this->assertTrue($task->isCompleted());
    }

    public function testRemoveTask()
    {
        /** @var Project $project */
        $project = make(Project::class);
        $project->addTask('Test task');
        $task = $project->tasks[0];

        $this->assertCount(1, $project->tasks);

        $project->removeTask($task->id);

        $this->assertCount(0, $project->tasks);
    }
}