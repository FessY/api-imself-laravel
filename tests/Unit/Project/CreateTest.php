<?php

namespace Tests\Unit\Project;

use App\Entities\Project\Project;
use App\Entities\User\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    public function testNew()
    {
        $user = make(User::class);

        $project = Project::new(
            $userId = $user->id,
            $title = 'Test project'
        );

        $this->assertNotEmpty($project);
        $this->assertEquals($userId, $project->user_id);
        $this->assertEquals($title, $project->title);
        $this->assertTrue($project->isActive());
    }
}