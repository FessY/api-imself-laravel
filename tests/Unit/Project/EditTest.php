<?php

namespace Tests\Unit\Project;

use App\Entities\Project\Project;
use App\Entities\User\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EditTestTest extends TestCase
{
    use RefreshDatabase;

    public function testEditTitle()
    {
        $user = make(User::class);
        $project = Project::new(
            $user->id,
            'Test project'
        );

        $project->setTitle($title = 'Test project 2');

        $this->assertEquals($title, $project->title);
        $this->assertTrue($project->isActive());
    }

    public function testMakeCompleted()
    {
        $user = make(User::class);
        $project = Project::new(
            $user->id,
            'Test project'
        );

        $project->changeStatus(Project::STATUS_COMPLETED);

        $this->assertTrue($project->isCompleted());
        $this->assertFalse($project->isActive());
    }
}