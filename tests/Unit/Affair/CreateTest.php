<?php

namespace Tests\Unit\Affair;

use App\Entities\Affair;
use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    public function testNew()
    {
        /** @var User $user */
        $user = make(User::class);
        $affair = Affair::new(
            $userId = $user->id,
            $title = 'Test title',
            $days = [1 => true, 2 => true, 5 => true],
        );

        $this->assertTrue($affair->isActive());
        $this->assertEquals($userId, $affair->user_id);
        $this->assertEquals($title, $affair->title);
        $this->assertSame($days, $affair->stages);
    }
}
