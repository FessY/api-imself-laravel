<?php

namespace Tests\Unit\Affair;

use App\Entities\Affair;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EditTest extends TestCase
{
    use RefreshDatabase;

    public function testSetTitle()
    {
        /** @var Affair $affair */
        $affair = make(Affair::class);

        $affair->setTitle($title = 'Test title');

        $this->assertEquals($title, $affair->title);
    }

    public function testChangeDay()
    {
        $date = Carbon::now();
        $dateString = $date->toDateString();

        /** @var Affair $affair */
        $affair = make(Affair::class);
        $affair->changeStage($date);

        $this->assertArrayHasKey($dateString, $affair->stages);

        $affair->changeStage($date);

        $this->assertArrayNotHasKey($dateString, $affair->stages);
    }
}
