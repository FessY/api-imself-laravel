<?php

namespace Tests\Unit\Task;

use App\Entities\Project\Project;
use App\Entities\Project\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EditTest extends TestCase
{
    use RefreshDatabase;

    public function testEdit()
    {
        /** @var Task $task */
        $task = make(Task::class, ['type' => Task::TYPE_LATER]);

        $this->assertTrue($task->isLater());

        $task->edit(
            $title = 'Test task',
            $type = Task::TYPE_TODAY,
        );

        $this->assertEquals($title, $task->title);
        $this->assertTrue($task->isToday());
    }

    public function testComplete()
    {
        /** @var Task $task */
        $task = make(Task::class, ['type' => Task::TYPE_TODAY]);

        $this->assertTrue($task->isActive());

        $task->complete();

        $this->assertTrue($task->isCompleted());
    }

    public function testActivate()
    {
        /** @var Task $task */
        $task = make(Task::class, ['type' => Task::TYPE_COMPLETED]);

        $this->assertTrue($task->isCompleted());

        $task->activate();

        $this->assertTrue($task->isActive());
    }

    public function testChangeType()
    {
        /** @var Task $task */
        $task = make(Task::class, ['type' => Task::TYPE_TODAY]);

        $this->assertTrue($task->isActive());

        $task->changeType(Task::TYPE_COMPLETED);

        $this->assertTrue($task->isCompleted());

        $task->changeType(Task::TYPE_TODAY);

        $this->assertTrue($task->isActive());
    }

    public function testAssignProject()
    {
        /** @var Task $task */
        $task = make(Task::class, ['type' => Task::TYPE_TODAY]);
        /** @var Project $project */
        $project = make(Project::class, ['type' => Project::STATUS_ACTIVE]);

        $this->assertNotEquals($project->id, $task->project_id);

        $task->assignProject($project->id);

        $this->assertEquals($project->id, $task->project_id);
    }
}
