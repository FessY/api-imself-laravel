<?php

namespace Tests\Unit\Task;

use App\Entities\Project\Task;
use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    public function testNew()
    {
        $user = make(User::class);

        $task = Task::newWithoutProject(
            $userId = $user->id,
            $title = 'Test task',
        );

        $this->assertEquals($userId, $task->user_id);
        $this->assertEquals($title, $task->title);
        $this->assertTrue($task->isActive());
        $this->assertTrue($task->isToday());
    }

    public function testNewWithType()
    {
        $user = make(User::class);

        $task = Task::newWithoutProject(
            $userId = $user->id,
            $title = 'Test task',
            $type = Task::TYPE_LATER,
        );

        $this->assertEquals($userId, $task->user_id);
        $this->assertEquals($title, $task->title);
        $this->assertTrue($task->isActive());
        $this->assertTrue($task->isLater());
    }
}
